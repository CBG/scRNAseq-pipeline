#scRNA-seq_pipeline
#author: Mariana Ribeiro

#!/bin/sh
#run in working directory in Iris (HPC Cluster)

export PATH=~/sratoolkit/bin:$PATH
export PATH=~/FastQC:$PATH
export PATH=~/STAR/bin:$PATH

source file.config

#create copy of parametrs run in pipeline
timestamp=`date +%Y-%m-%d.%H:%M:%S`
cp file.config "$timestamp"_file.config
awk '{ gsub("=",": ", $0); print $0 }' file.config > config.yml

mkdir -p fastq
mkdir -p FastQC
mkdir -p results_STAR

## get data raw data from SRA
#.txt with SRA accessions
if [[ -a "$sra_ac" ]]; then
    for i in $(cat $sra_ac); do
        while true; do
            vdb-validate $i
            RC=$?
            if [ $RC -ne 0 ]; then
                prefetch $i
            else
                break
            fi
        done
        fastq-dump --split-files ~/ncbi/public/sra/$i.sra
        rm -r ~/ncbi/public/sra/*
    done
else
    wget "$url"
    #decompress all files
    for t in *.tar; do tar -x "$t"; done
    for gz in *.gz; do gzip -d "$gz"; done
fi

## run FastQC for quality control: trimming based on https://bioinformatics-core-shared-training.github.io/cruk-summer-school-2018/Introduction/SS_DB/Materials/Practicals/Practical1_fastQC_DB.html
for i in `ls *.fastq | sed 's/_[12].fastq//g' | sort -u`; do
    if [ "$paired" eq 1 ]; then
        fastqc -o ./FastQC $i_1.fastq $i_2.fastq
        cutadapt -j "$thread" -m 10 -q 20 -o $i_trimmed_1.fastq -p $i_trimmed_2.fastq $i_1.fastq $i_2.fastq
    else
        fastqc -o ./FastQC $i.fastq
        cutadapt -j "$thread" -m 10 -q 20 -o $i_trimmed.fastq $i.fastq
    fi
done
rm ./FastQC/*.html

## make STAR index - for better alligment make size of reads (sjdbOverhang) equal to lenght of sequence
if [ ! -d "$star_index" ]; then
    STAR --runThreadN "$thread" \
    --runMode genomeGenerate \
    --genomeFastaFiles $genome \
    --genomeDir $index \
    --sjdbGTFfile $gtf \
    --sjdbOverhang "$overhang"	#(adjust this value as described above)
fi


#------------- for UMI-based data ---------------------

if [ "$UMI" eq 1 ]; then
    ## Pool all cell barcode fastq files (R1) and UMI fastq files (R2) into one to make the processing easier
    cat *${r1_unique}*.fastq > "$proj_id"_R1.fastq;
    cat *${r2_unique}*.fastq > "$proj_id"_R2.fastq;
    ## Identify correct cell barcodes
    #5’ end of the read (this can be changed with --3prime) C-Cell barcode  U-UMI (see file.config)
    umi_tools whitelist --stdin "$proj_id"_R1.fastq \
    --bc-pattern="$pattern" \
    --expect-cells="$ncells" \
    --plot-prefix=expect_whitelist \
    --log2stderr > whitelist.txt
    ## Extract barcdoes and UMIs and add to read names
    #check paired reads
    umi_tools extract --bc-pattern="$pattern" \
    --stdin "$proj_id"_R1.fastq \
    --stdout "$proj_id"_R1_extracted.fastq \
    --read2-in "$proj_id"_R2.fastq \
    --read2-out="$proj_id"_R2_extracted.fastq \
    --filter-cell-barcode \
    --whitelist=whitelist.txt
    ## Map reads (tweaking based on https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5792058/bin/NIHMS902588-supplement-Supplemental.pdf)
    STAR --runThreadN "$thread" \
    --genomeDir $index \
    --genomeLoad LoadAndKeep \
    --readFilesIn "$proj_id"_R2_extracted.fastq \
    --readFilesCommand cat \
    --outFilterMultimapNmax 1 \
    --outFileNamePrefix ./results_STAR/ \
    --limitBAMsortRAM 20000000000 \
    --outSAMtype BAM SortedByCoordinate \
    --outFilterMultimapNmax 100 \
    --outFilterMismatchNmax 33 \
    --seedSearchStartLmax 12 \
    --alignSJoverhangMin 15 \
    --outFilterScoreMinOverLread 0.3 \
    --outFilterMatchNminOverLread 0 \
    --outFilterMatchNmin 0 \
    --outFilterType BySJout
    STAR --runThreadN "$thread" \
    --genomeDir $index \
    --genomeLoad Remove
    ## Assign reads to genes
    featureCounts -a $gtf -o UMI_assigned -R BAM ./results_STAR/Aligned.sortedByCoord.out.bam -T "$thread"
    samtools sort Aligned.sortedByCoord.out.bam.featureCounts.bam -o UMI_assigned_sorted.bam
    samtools index UMI_assigned_sorted.bam
    ## Count UMIs per gene per cell
    umi_tools count --per-gene --gene-tag=XT --assigned-status-tag=XS --per-cell -I UMI_assigned_sorted.bam -L UMI_count.log -S UMI_counts.tsv
else
    #------------- for non-UMI-based data ---------------------
    ## Map reads (tweaking based on https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5792058/bin/NIHMS902588-supplement-Supplemental.pdf)
    for f in `ls *trimmed*.fastq | sed 's/_[12].fastq//g' | sort -u`; do
        if [ "$paired" eq 1 ]; then
            STAR --runThreadN "$thread" \
    	    --genomeDir $index \
    	    --genomeLoad LoadAndKeep \
    	    --readFilesIn ${f}_1.fastq ${f}_2.fastq \
    	    --outFileNamePrefix ./results_STAR/${f} \
    	    --limitBAMsortRAM 20000000000 \
    	    --outSAMtype BAM SortedByCoordinate \
    	    --outFilterMultimapNmax 100 \
    	    --outFilterMismatchNmax 33 \
    	    --seedSearchStartLmax 12 \
    	    --alignSJoverhangMin 15 \
    	    --outFilterScoreMinOverLread 0.3 \
    	    --outFilterMatchNminOverLread 0 \
    	    --outFilterMatchNmin 0 \
    	    --outFilterType BySJout
        else
            STAR --runThreadN "$thread" \
    	    --genomeDir $index \
    	    --genomeLoad LoadAndKeep \
    	    --readFilesIn ${f}.fastq \
    	    --outFileNamePrefix ./results_STAR/${f} \
    	    --limitBAMsortRAM 20000000000 \
    	    --outSAMtype BAM SortedByCoordinate \
    	    --outFilterMultimapNmax 100 \
    	    --outFilterMismatchNmax 33 \
    	    --seedSearchStartLmax 12 \
    	    --alignSJoverhangMin 15 \
    	    --outFilterScoreMinOverLread 0.3 \
    	    --outFilterMatchNminOverLread 0 \
    	    --outFilterMatchNmin 0 \
    	    --outFilterType BySJout
    	fi
    STAR --runThreadN "$thread" \
	--genomeDir $index \
	--genomeLoad Remove
	## Count reads per gene per sample
	#pip install --user HTSeq
    for f in `ls ./results_STAR/*.bam | sed 's/Aligned.sortedByCoord.out.bam//g' | sort -u`; do
        python -m HTSeq.scripts.count -f "$format" -r "$order" -s "$stranded" ${f}Aligned.sortedByCoord.out.bam $gtf > ${f}.txt
        head -n -5 ${f}.txt > ${f}_final.txt
    done
fi

multiqc --ignore *_R2* --export .
multiqc --export FastQC
multiqc --export results_STAR

mv *.fastq ./fastq