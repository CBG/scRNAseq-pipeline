# scRNAseq-pipeline
# Author: Mariana Ribeiro

# Seurat v.3 integration
# https://satijalab.org/seurat/

library(yaml)
library(dplyr)
library(Seurat)
library(SeuratWrappers)
library(ggplot2)
library(Matrix)

#setwd(wkdir)
config <- yaml.load_file("config.yml")
dir.create("Rgraphs")

#Bind count files into count matrix
count_matrix<-data.frame()[ , ]
for (f in list.files(pattern = "\\_final.txt$")) {
  mtr<-read.table(f, row.names = 1, col.names = c("Gene",f))
  if(nrow(count_matrix)!=0) {
    count_matrix<-cbind.data.frame(count_matrix,mtr)
  } else {
    count_matrix<-data.frame(mtr)
  }
}
write.csv(count_matrix, paste(Sys.time(), paste0(proj_id, "counts.csv"), sep="_"))

# test for the first ten cells
count_matrix[1:10]

#Filtering low-quality cells
counts_per_cell <- Matrix::colSums(count_matrix)
counts_per_gene <- Matrix::rowSums(count_matrix)
genes_per_cell <- Matrix::colSums(count_matrix>0) # count gene only if it has non-zero reads mapped.
cells_per_gene <- Matrix::rowSums(count_matrix>0) # only count cells where the gene is expressed

  #histograms
pdf(paste(Sys.time(), "low-quality_hist.pdf", sep="_"), width = 12, height = "8")
hist(log10(counts_per_cell+1),main='counts per cell',col='light blue')
hist(log10(genes_per_cell+1), main='genes per cell', col='light blue')
hist(log10(counts_per_gene+1), main='counts per gene', col='light blue')
plot(sort(genes_per_cell), xlab='cell', log='y', main='genes per cell (ordered)')
plot(counts_per_cell, genes_per_cell, log='xy', col='light blue')
title('counts vs genes per cell')
dev.off()


# Initialize the Seurat object with the raw data
seurat_obj <- CreateSeuratObject(counts = count_matrix, project = config$proj_id, min.cells = config$min_cells, min.features = config$min_features)

# QC metrics. The [[ operator can add columns to object metadata
seurat_obj[["percent.mt"]] <- PercentageFeatureSet(object = seurat_obj, pattern = "^MT")
#mito.genes <- grep(pattern = "mt-", x = rownames(x = seurat_obj@Gene), value = TRUE)
head(x = seurat_obj@meta.data, 5)

# Visualize QC metrics as a violin plot
pdf(paste(Sys.time(), "violin_plot.pdf", sep="_"), width = 12, height = "8")
VlnPlot(object = seurat_obj, features = c("nFeature_RNA", "nCount_RNA", "percent.mt"), ncol = 3)
dev.off()

# FeatureScatter to visualize feature-feature relationships
pdf(paste(Sys.time(), "scatter_plot.pdf", sep="_"), width = 350, height = "350")
plot1 <- FeatureScatter(object = seurat_obj, feature1 = "nCount_RNA", feature2 = "percent.mt") 
plot2 <- FeatureScatter(object = seurat_obj, feature1 = "nCount_RNA", feature2 = "nFeature_RNA") 
CombinePlots(plots = list(plot1,plot2))

# Filtering - subset data
seurat_obj <- subset(x = seurat_obj, subset = nFeature_RNA > config$minFeat_RNA & nFeature_RNA < config$maxFeat_RNA & percent.mt < config$percent_mt)

#Scalling the data
seurat_obj <- SCTransform(object = seurat_obj, vars.to.regress = config$vars_regress)

# plot variable features
plot1 <- VariableFeaturePlot(object = seurat_obj)
plot2 <- LabelPoints(plot = plot1, points = top10, repel = TRUE, xnudge=0, ynudge=0)
pdf(paste(Sys.time(), "deg_plot.pdf", sep="_"), width = 14, height = "8")
CombinePlots(plots = list(plot1,plot2))
dev.off()

#PCA
seurat_obj <- RunPCA(npcs = config$npcs, features = VariableFeatures(object = seurat_obj))
# Examine and visualize PCA results a few different ways
print(x = seurat_obj[['pca']], dims = 1:5, nfeatures = 5)
pdf(paste(Sys.time(), "pca.pdf", sep="_"), width = 10, height = "8")
VizDimLoadings(object = seurat_obj, dims = config$dims_pca, reduction = "pca", nfeatures = config$feat_pca)
dev.off()
pdf(paste(Sys.time(), "pca_cluster.pdf", sep="_"), width = 8, height = "8")
DimPlot(object = seurat_obj, reduction = 'pca')
dev.off()

#heatmap
pdf(paste(Sys.time(), "heatmap.pdf", sep="_"), width = 10, height = "100")
DimHeatmap(object = seurat_obj, dims = config$dims_heat, cells = config$cells, balanced = TRUE)
dev.off()

#Determine the 'dimensionality' of the dataset
if (config$jackstraw==1) {
    seurat_obj <- JackStraw(seurat_obj, num.replicate = config$nreplicate)
    seurat_obj <- ScoreJackStraw(object = seurat_obj, dims = config$dims_jackstraw)
    pdf(paste(Sys.time(), "jackstraw.pdf", sep="_"), width = 10, height = "50")
    JackStrawPlot(object = seurat_obj, dims = config$plot_jack_dim)
    ElbowPlot(object = seurat_obj)
    dev.off()
} else {
  pdf(paste(Sys.time(), "elbowplot.pdf", sep="_"), width = 10, height = "50")
  ElbowPlot(object = seurat_obj)
  dev.off()
}

# batch effect correction - fastMNN integration
groups_raw <- read.table(config$condition, sep=",", header = FALSE)
groups <- as.character(as.matrix(groups_raw[2]))
names(groups) <- colnames(x = seurat_obj)
seurat_obj <- AddMetaData(object = seurat_obj, metadata = groups, col.name = 'condition')
seurat_obj <- RunFastMNN(object.list = SplitObject(seurat_obj, split.by = 'condition'))
seurat_obj <- RunUMAP(seurat_obj, reduction = 'mnn', dims = config$dims_batch)

#Clustering
seurat_obj <- FindNeighbors(object = seurat_obj, reduction = 'mnn', dims = config$dims_clus)
seurat_obj <- FindClusters(object = seurat_obj, resolution = config$res)
# Look at cluster IDs of the first 5 cells
head(x = Idents(object = seurat_obj), 5)
write.table(as.matrix(Idents(object=pbmc)), file="clusters.txt", sep=",", col.names=FALSE)

#Visualizing with TSNE
seurat_obj <- RunTSNE(object = seurat_obj, reduction = config$reduct, dims = config$dims_clus)
pdf(paste(Sys.time(), "tsne.pdf", sep="_"), width = 10, height = "50")
TSNEPlot(object = seurat_obj, reduction = 'tsne'))
dev.off()

#Visualizing with UMAP
seurat_obj <- RunUMAP(object = seurat_obj, reduction = config$reduct, dims = config$dims_clus)
pdf(paste(Sys.time(), "umap.pdf", sep="_"), width = 10, height = "50")
DimPlot(object = seurat_obj, reduction = 'umap')
dev.off()


## Finding differentially expressed features (cluster biomarkers)
# find markers for every cluster compared to all remaining cells, report only the positive ones
seurat_obj.markers <- FindAllMarkers(object = config$cluster, only.pos = config$only_pos, min.pct = config$min_pct, logfc.threshold = config$logfc, test.use = config$test)
seurat_obj.markers %>% group_by(cluster) %>% top_n(n = config$n_markers, wt = config$wt)
write.csv(seurat_obj.markers, paste(Sys.time(), paste0(proj_id, "markers.csv"), sep="_"))

#visualizing marker expression
pdf(paste(Sys.time(), "cluster_violin.pdf", sep="_"), width = 10, height = "50")
VlnPlot(object = seurat_obj, features = config$feat_plot)
dev.off()
  #raw counts
pdf(paste(Sys.time(), "cluster_violin-counts.pdf", sep="_"), width = 10, height = "50")
VlnPlot(object = seurat_obj, features = config$feat_plot, slot = 'counts', log = TRUE)
dev.off()
pdf(paste(Sys.time(), "cluster_feature-counts.pdf", sep="_"), width = 10, height = "50")
FeaturePlot(object = seurat_obj, features = config$feat_plot)
dev.off()

#heatmap - top 10 markers (or all markers if less than 10) for each cluster
top10_markers <- seurat_obj.markers %>% group_by(cluster) %>% top_n(n = 10, wt = config$wt)
pdf(paste(Sys.time(), "cluster_heatmap.pdf", sep="_"), width = 8, height = "15")
DoHeatmap(object = seurat_obj, features = top10_markers$gene) + NoLegend()
dev.off()

### Assigning cell type identity to clusters
names(x = config$clus_ids) <- levels(x = seurat_obj)
seurat <- RenameIdents(object = seurat_obj, config$clus_ids)

#umap visualization
plot <- DimPlot(object = seurat_obj, reduction = config$clus_id_reduct, label = TRUE, label.size = 4.5) + xlab(config$xlab) + ylab(config$ylab) + 
  theme(axis.title = element_text(size = 18), legend.text = element_text(size = 18)) + 
  guides(colour = guide_legend(override.aes = list(size = 10)))
ggsave(filename = paste(Sys.time(), "cluster-id_umap.pdf", sep="_"), height = 7, width = 12, plot = plot)


#move *.pdf
Rgraphs <- paste(Sys.time(), "Rgraphs", sep = "_")
for (g in list.files(pattern = ".pdf")) {
  file.move(g, Rgraphs)
}

#saveRData and parameters
save.image(file = paste0(paste(Sys.time(), proj_id, sep = "_"), ".RData"))
saveRDS(seurat_obj, file = paste0(paste(Sys.time(), proj_id, sep = "_"), ".rds"))
